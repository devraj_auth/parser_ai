from django.contrib import admin
from .models import QnA
from .forms import QnaForm

# Register your models here.
class QnaAdmin(admin.ModelAdmin):
    list_display = ['question', 'answer']
    form = QnaForm
    list_filter = ['grade', 'subject', 'set']
    search_fields = ['grade', 'subject', 'set']
admin.site.register(QnA, QnaAdmin)
