from django import forms
from .models import QnA
import spacy



class QnaForm(forms.ModelForm):
    class Meta:
        model = QnA
        fields = '__all__'

class SearchForm(forms.ModelForm):
    class Meta:
        model = QnA
        fields = ['grade', 'subject', 'set']

class QnaUpdateForm(forms.ModelForm):
    class Meta:
        model = QnA
        fields = '__all__'

