from django.db import models

class QnA(models.Model):
    grade = models.CharField(max_length = 200)
    subject = models.CharField(max_length = 200)
    set = models.CharField(max_length = 200)
    question = models.CharField(max_length = 200)
    answer = models.CharField(max_length = 200)

    def __str__(self):
        return self.question + '' + self.answer

