from django.contrib import admin
from django.urls import path
from parserapp import views 

urlpatterns = [
    # path('', views.homeView),
    path('list/', views.QnaListView),
    path('', views.add_QueryView, name="add_Query"),
    # path('add_Answer/', views.add_AnswerView, name = "add_Answer"),
    path('update_Query/<str:pk>/', views.update_QueryView, name = 'query_update'),
    path('delete_Query/<str:pk>/', views.delete_QueryView, name = 'query_delete'),


]
