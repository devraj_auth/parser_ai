from django.shortcuts import render, redirect
from .models import QnA
from .forms import QnaForm, SearchForm, QnaUpdateForm
from django.core.paginator import Paginator

from parserapp.pos import parse
# Create your views here.


# question = "get it from form"
# print(instance.queryset)



# store that parsed thing in database using model



def homeView(request):
    title = "parser tagger"
    form = "Devraj. THis is a parser tagger."
    context = {
        "title": title,
        "test": form,
    }
    return render(request, 'home.html', context)
#list query with paginator added.
def QnaListView(request):
    title = "list of queries"
    form = SearchForm(request.POST or None)
    queryset = QnA.objects.all()
    paginator = Paginator(queryset, 10)
    page_num = request.GET.get('page', 1)
    queryset = paginator.page(page_num)
    context = {
        "title": title,
        "queryset": queryset,
        "form": form
    }
    #search and filter
    if request.method == 'POST':
        queryset = QnA.objects.filter(
        
            grade=form['grade'].value(),
            subject=form['subject'].value(),
            set=form['set'].value()
        )

        
        context = {
            "form": form,
            "title": title,
            "queryset": queryset,
        }

     
    return render(request, 'Qna_List.html', context)

# # instances
# for instance in queryset:
#     print(instance)

#add query
def add_QueryView(request):
    form = QnaForm(request.POST or None)
    if form.is_valid():
        form.save()

        

        
        return redirect('/list')
    context = {
        "form": form,
        "title": "Add Query"
    }

    

    return render(request, "add_Query.html", context)
    

# edit and update view queries.

def update_QueryView(request, pk):
    queryset = QnA.objects.get(id=pk)  
    form = QnaUpdateForm(instance = queryset)
    if request.method == 'POST':
        form = QnaUpdateForm(request.POST, instance=queryset)
        if form.is_valid():
            form.save()
            return redirect('/list')

    context = {
        'form': form
    } 
    return render(request, 'add_Query.html', context)

#delete queries.
def delete_QueryView(request, pk):
    queryset = QnA.objects.get(id=pk)
    queryset.delete()
    return redirect('/list')

queryset = QnA.objects.values_list()
for i in range(len(queryset)):
    q = parse(queryset[i][4])
    print(q)
    ans = parse(queryset[i][5])
    print(ans)

 